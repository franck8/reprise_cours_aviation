package fr.toulouse.home.rework.aviationBeans.data.fb;

import java.time.LocalDateTime;

public class Flight2 {

	private Long id;
	private LocalDateTime departure;
	private String origin;
	private String destination;
	private Long plane;

	Flight2() {
		this(null, null, null, null);
	}

	public Flight2(LocalDateTime departure, String origin, String destination, Long plane) {
		this.departure = departure;
		this.origin = origin;
		this.destination = destination;
		this.plane = plane;
	}

	public Flight2(Long id, LocalDateTime departure, String origin, String destination, Long plane) {
		super();
		this.id = id;
		this.departure = departure;
		this.origin = origin;
		this.destination = destination;
		this.plane = plane;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDeparture() {
		return departure;
	}

	public void setDeparture(LocalDateTime departure) {
		this.departure = departure;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Long getPlane() {
		return plane;
	}

	public void setPlane(Long plane) {
		this.plane = plane;
	}

	@Override
	public String toString() {
		return "Flight2 [id=" + id + ", departure=" + departure + ", origin=" + origin + ", destination=" + destination
				+ ", plane=" + plane + "]";
	}

}
