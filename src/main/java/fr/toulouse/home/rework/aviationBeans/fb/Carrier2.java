package fr.toulouse.home.rework.aviationBeans.fb;

import java.time.LocalDateTime;
import java.util.Set;

public abstract class Carrier2 {

	private String name;
	private LocalDateTime foundation;
	private Coordinate2 office;

	Carrier2() {
		this(null);
	}

	public Carrier2(String name) {
		this.name = name;
	}
	
	public abstract LocalDateTime getNow(); 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getFoundation() {
		return foundation;
	}

	public void setFoundation(LocalDateTime foundation) {
		this.foundation = foundation;
	}

	public Coordinate2 getOffice() {
		return office;
	}

	public void setOffice(Coordinate2 office) {
		this.office = office;
	}

	public Set<Plane2> getPlanes() {
		return planes;
	}

	public void setPlanes(Set<Plane2> planes) {
		this.planes = planes;
	}

	private Set<Plane2> planes;

	@Override
	public String toString() {
		return ("name = " + name + "office = " + office + "valable le " + getNow());
	}
	
	
	

}
