package fr.toulouse.home.rework.aviationBeans.data.fb;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

public class PlanningDAO2 {

	private NamedParameterJdbcTemplate template;
	private static final String owncode = "TLS";

	public void setTemplate(NamedParameterJdbcTemplate template) {
		this.template = template;
	}

	public void init() {
		String delflight = "DELETE FROM flight";
		template.getJdbcOperations().execute(delflight);
		String delplane = "DELETE FROM plane";
		template.getJdbcOperations().execute(delplane);
	}

	public long insertplane(String model, String immat) {
		String insPlane = "INSERT INTO plane (model, immat) VALUES(:model, :immat)";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("A380", "fr12365");
		data.addValue("A307", "fr2458");
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		template.update(insPlane, data, h);
		return h.getKey().longValue();
	}

	public long insertflight(Flight2 f) {
		String insFlight = "INSERT INTO flight (departure, destination, origin, plane) VALUES(:departure, :destination, :origin, :plane)";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("departure", f.getDeparture());
		data.addValue("destination", f.getDestination());
		data.addValue("origin", f.getOrigin());
		data.addValue("plane", f.getPlane());
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		template.update(insFlight, data, h);
		return h.getKey().longValue();
	}

	public List<Flight2> getFlightByOrigin(String origin) {
		String selFlight = "SELECT * FROM flight WHERE origin like :origin";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("origin", origin);
		List<Flight2> res = new ArrayList<Flight2>();
		for (Map<String, Object> m : template.queryForList(selFlight, data)) {
			res.add(new Flight2(new Long((Integer) m.get("id")), ((Timestamp) m.get("departure")).toLocalDateTime(),
					(String) m.get("destination"), (String) m.get("origin"), (new Long((Integer) m.get("plane")))));

		}
		return res;
	}

	public void addJourney(String destination, String immat, LocalDateTime dep2, LocalDateTime dep1) {

		String selJourneyPlane = "SELECT * FROM plane WHERE immat like :immat";
		MapSqlParameterSource data = new MapSqlParameterSource();
		data.addValue("immat", immat);
		long id = (long) template.queryForObject(selJourneyPlane, data, Long.class);

		insertflight(new Flight2(dep1, owncode, destination, id));
		insertflight(new Flight2(dep2, destination, owncode, id));

	}
}
