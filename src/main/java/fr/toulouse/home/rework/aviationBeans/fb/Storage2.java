package fr.toulouse.home.rework.aviationBeans.fb;

import java.util.ArrayList;
import java.util.List;

public class Storage2 {

	List<Double> volumes;

	Storage2() {
		this(new ArrayList<Double>());
	}
	
	public Storage2(List<Double> volumes) {
		this.volumes = volumes;
	}

	public void addVolume(double v) {
		volumes.add(v);
	}

	public List<Double> getVolume() {
		return volumes;
	}

	public int getNumber() {
		return volumes.size();
	}
	
	public void setVolumes(List<Double> volumes) {
		this.volumes = volumes;
	}

	@Override
	public String toString() {
		return "Storage [volumes=" + volumes + "]";
	}
}
