package fr.toulouse.home.rework.aviationBeans.data.fb;

import java.time.LocalDateTime;
import org.aspectj.lang.JoinPoint;

public class LogAspect2 {

	public void enter(JoinPoint jp) {
		System.out.println(LocalDateTime.now() + "enter" + jp.getSignature().getDeclaringTypeName()); 
	}

	public void exit() {

		System.out.println(LocalDateTime.now() + "exit"); 
	}
}
