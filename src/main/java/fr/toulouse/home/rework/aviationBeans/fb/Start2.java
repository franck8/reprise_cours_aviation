package fr.toulouse.home.rework.aviationBeans.fb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

public class Start2 {

	public static void main(String[] args) {
		
		AbstractApplicationContext context = new FileSystemXmlApplicationContext("classpath:context.xml");
		Resource ad = context.getResource("classpath:ad.txt"); 
		try {

			File adfile = ad.getFile(); 																   // generation d'un conteneur "adfile" pour l'obtention du NOM du fichier ad.txt
			BufferedReader r = new BufferedReader(new FileReader(adfile)); 								   // generation d'un conteneur "r" avec .txt ouvert, il ne manque plus que la lecture																
			String l; 																					   // creation de variable
			while ((l = r.readLine()) != null)															   // va dans le fichier contenu et si pas possible de lire - renvoit NULL
				System.out.println(l + "\n");
			r.close(); 																					   // fermeture du fichier
		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
		
		Coordinate2 c1 = new Coordinate2(154,132,2500);
		System.out.println(c1);
		Coordinate2 c2 = new Coordinate2(-45,56,4580);
		System.out.println(c2);
		Coordinate2 c3 = (Coordinate2)context.getBean("towerCoord");
		System.out.println(c3);
		String c4 = (String)context.getBean("airportName");
		System.out.println(c4);
		Integer c5 = (Integer)context.getBean("hangarCapacity");
		System.out.println(c5);
		Storage2 sel = (Storage2)context.getBean("storage");
		System.out.println(sel);
		Plane2 pl = (Plane2)context.getBean("plane");
		System.out.println(pl);
		LocalDateTime fd = (LocalDateTime)context.getBean("foundation");
		System.out.println(fd);
		Carrier2 cr = (Carrier2) context.getBean("carrier1");
		System.out.println(cr);
		
	
	}

}
