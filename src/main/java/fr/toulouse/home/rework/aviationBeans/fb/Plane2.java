package fr.toulouse.home.rework.aviationBeans.fb;

public class Plane2 {

	private boolean available;
	private String model;

	Plane2() {
		this(true, null);
	}

	public Plane2(boolean available, String model) {
		this.available = available;
		this.model = model;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Plane [available=" + available + ", model=" + model + "]";
	}

	public void init() {
		System.out.println("init");
	}

	public void destroy() {
		System.out.println("destroy");

	}

}
