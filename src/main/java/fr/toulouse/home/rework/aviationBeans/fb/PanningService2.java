package fr.toulouse.home.rework.aviationBeans.fb;

import java.time.LocalDateTime;

import fr.toulouse.home.rework.aviationBeans.data.fb.Flight2;
import fr.toulouse.home.rework.aviationBeans.data.fb.PlanningDAO2;

public class PanningService2 {

	private PlanningDAO2 dao;

	public void setDao(PlanningDAO2 dao) {
		this.dao = dao;
	}

	public void execute() {
		System.out.println("Planning:");
		dao.insertplane("A307", "FR125254");
		dao.insertplane("A580", "FR16HG");
		System.out.println(dao.insertplane("A103", "FR1977JH"));

		long p1 = dao.insertplane("A546", "FR2548");
		long p2 = dao.insertplane("A234", "FR9145");

		dao.insertflight(new Flight2(LocalDateTime.of(2018, 12, 25, 14, 23), "TLS", "MRL", p1));
		dao.insertflight(new Flight2(LocalDateTime.of(2017, 11, 24, 18, 32), "PRS", "TLS", p2));
		System.out.println(dao.getFlightByOrigin("TLS"));
		dao.addJourney("TLS","FR2548", LocalDateTime.of(2017, 12, 25, 14, 23), LocalDateTime.of(2017, 11, 24, 18, 32));

	}
}