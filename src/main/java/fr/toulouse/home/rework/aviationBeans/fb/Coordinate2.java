package fr.toulouse.home.rework.aviationBeans.fb;

import java.io.Serializable;

public class Coordinate2 implements Serializable {

	/*
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double longitude;
	private double latitude;
	private double altitude;

	Coordinate2() {
		this(0, 0, 0);
	}

	public Coordinate2(double longitude, double latitude, double altitude) {
		super();
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Coordinate [longitude=" + longitude + ", latitude=" + latitude + ", altitude=" + altitude + "]";
	}

}
